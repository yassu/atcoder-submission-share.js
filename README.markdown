AtCoder Submission Share
==========================

このプロジェクトは, 自分のためのatcoderのsubmissionの共有用のツイートボタンを表示するプロジェクトです.

![photo1](assets/photo1.png)

![photo2](assets/photo2.png)

## LICENSE

[MIT](https://gitlab.com/yassu/atcoder-submission-share.js/-/blob/master/LICENSE)
