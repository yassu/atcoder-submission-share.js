let elem = document.getElementsByClassName('h2')[0];
let probTitle = ''
if (document.getElementsByClassName('table-bordered')[0].children[0].rows[1].children[1].children[1] != undefined) {
  probTitle = document.getElementsByClassName('table-bordered')[0].children[0].rows[1].children[1].children[1].text;
} else {
  probTitle = document.getElementsByClassName('table-bordered')[0].children[0].rows[1].children[1].innerText;
}
const status = document.getElementsByClassName('table-bordered')[0].children[0].rows[6].children[1].children[0].innerText;
const contestName = location.href.split('/')[4].toUpperCase();
const twImageUrl = chrome.runtime.getURL('assets/icon-16px.png');

const twtext = encodeURIComponent(`「${probTitle} (${contestName})」の提出結果は${status}でした！
詳細はこちら↓
`);
elem.insertAdjacentHTML('afterend',
  `
    <p><a class="twitter-share-button" href="https://twitter.com/share?text=${twtext}&url=${location.href}" data-dnt="true" data-lang="ja" target="_blank">
      <img src='${twImageUrl}' alt='twitterへ投稿' style="display: block; width: 38px; height: 38px; object-fit: contain;">
    </a></p>
  `
);
